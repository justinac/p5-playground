const canvasWidth   = window.innerWidth / 2
const canvasHeight  = window.innerHeight / 2

function setup() {
    createCanvas(canvasWidth, canvasHeight);
    background(255, [1]);
}

i = 0;
function draw() {
    i += 0.1;
    if (mouseIsPressed) {
        fill(0);
    } else {
        fill(255);
    }
    ellipse((sin(i) * 200) + canvasWidth / 2, (cos(i) * 200) + canvasHeight / 2, 50, 50);
}
